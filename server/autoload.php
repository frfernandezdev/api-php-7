<?php
	// spl_autoload_register function for manage php files in the project 
	// funcion spl_autoload_register para manejar archivos php en el proyecto 
	spl_autoload_register(function($class){
		$prefix = 'server\\'; 
		// name of the folder where the php functionalities are contained
		// nombre de la carpeta donde se encuentra la funcionalidades de php
		$base_dir = __DIR__ . '/'; 
		// name of the project is base folder
		// nombre de la base del proyecto 
		$len = strlen($prefix); 
		// function for length of string
        // funcion para obtener la longitud 
		if(strncmp($prefix, $class, $len) !== 0) {   
			return;
        }
		// function to compare the name of the folder with the files to be routed, of being true cancels the execution of the function
		// función para comparar el nombre de la carpeta con los archivos a enrutar, de ser verdadero cancela la ejecucion de la funcion
		$relative_class = substr($class, $len);
		// function to subtract the name of the file from its prefix
		// función para restar el nombre del archivo de su prefijo 
		$file = $base_dir . str_replace('\\','/', $relative_class). '.php';
		// build the url of the file to be routed
		// construir la url del archivo a enrutar
        if(file_exists($file)) {
			require_once $file;
		}else {
			trigger_error('Error not found file PHP');
		}
		// se ejecuta la funcion file_existes y se le pasa por parametros, si existe se se llama a dicho archivo de lo contrario se invoca a un error
		// the file_exists function is executed and it is passed through parameters, if it exists, that file is called otherwise an error is invoked
	});
    
	error_reporting(E_ALL);
	// report errors in file
	// reporta errores en el archivo
?>