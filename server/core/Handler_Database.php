<?php  

    namespace server\core;
    
    include_once dirname(__DIR__).'/config/config.php';

    use PDO;
    use server\core\Handler_SQLBuilder;
    use server\core\APIException;

    /**
	* Handler of Errors
	*/
    class Handler_Database extends PDO {

        private $driver		=	DB['DB_SERVER'];
		private $host		=	DB['DB_HOST_NAME'];
		private $dbname		=	DB['DB_DATABASE'];
		private $user		=	DB['DB_USER_NAME'];
		private $pass		=	DB['DB_PASSWORD'];
		private $charset	=	DB['DB_CHARSET'];
        private $sql;

        protected static $_instance = null;

		public static function getInstance() {
			if(!isset(self::$_instance))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function __construct() {
            $db = "{$this->driver}:dbname={$this->dbname};host={$this->host};chatset={$this->charset}";
			try {
				parent::__construct($db, $this->user, $this->pass, DB['DB_OPTIONS']);
			}catch (PDOAPIException $e) {
				throw new APIException($e->getCode(), $e->getMessage());
			}
        }

        public function getLastQuery() {
            return $this->sql;
        }

        public function findQuery(Handler_SQLBuilder $query) {
            $this->sql = $query->getSQL();
            $find = $this->prepare($this->sql);
            if($find && $find->execute())
                return $find->fetchAll(PDO::FETCH_ASSOC);
            else if($this->errorInfo())
                throw new APIException(implode(" ",$this->errorInfo())."SQL:".$this->sql);
            else return false;
        }

        public function findOneQuery(Handler_SQLBuilder $query) {
            $this->sql = $query->getSQL();
            $find = $this->prepare($this->sql);
            if($find && $find->execute())
                return $find->fetch(PDO::FETCH_ASSOC);
            else if($this->errorInfo())
                throw new APIException(implode(" ",$this->errorInfo())."SQL:".$this->sql);
            else return false;
        }

        public function insertQuery(Handler_SQLBuilder $query) {
            $this->sql = $query->getSQL();
            $insertTo = $this->prepare($this->sql);
            if($insertTo && $insertTo->execute(array_values($query->getValues())))
                return $this->lastInsertId();
            else if($this->errorInfo())
                throw new APIException(implode("", $this->errorInfo())."SQL:".$this->sql);
            else return false;
        }

        public function updateQuery(Handler_SQLBuilder $query) {
            $this->sql = $query->getSQL();
            $update = $this->prepare($this->sql);
            if($update && $update->execute(array_values($query->getValues())))
				return true;
			else if($this->errorInfo())
				throw new APIException(implode("", $this->errorInfo())."SQL:".$this->sql);
            else return false;
        }        

        public function deleteQuery(Handler_SQLBuilder $query)  {
            $this->sql = $query->getSQL();
            $delete = $this->exec($this->sql);
            if($delete && $delete != false)
                return true;
            else if($this->errorInfo())
                throw new APIException(implode(" ",$this->errorInfo())."SQL:".$this->sql);
            else return false; 
        }
        
    }