<?php 

    namespace server\core;

    use server\core\Handler_SQLBuilder;
    use server\core\APIException;

    class Handler_Authentications {

        private $headers;
        private $session;
        private $token;
        private $tokenSystem;
        private $logged;
        private $user;

        protected function __construct() {            
            $this->session = $_SESSION;

            $this->logged = false;

            if(array_key_exists('token', $this->session) && array_key_exists('tokenSystem', $this->session)) {
                $this->token = $this->session['token'];
                $this->tokenSystem = $this->session['tokenSystem'];
                if($this->user = $this->getUser())
                    return $this->user;
                else return false;
            }else return false;
        }   

        private function getUser() {
            $binnacle = new Handler_SQLBuilder('table_binnacle');
            $users = new Handler_SQLBuilder('table_users');
            $query = array(
                '=' => array(
                    'status' => 0,
                    'token' => $this->token,
                    'tokensystem' => $this->tokenSystem,
                ),
                '<=' => array(
                    'expires' => time()
                )
            );
            $binnacle->findOne($query);
            $response = $binnacle->execute();
            var_dump($response);
            if(is_array($response) && count($response)>0) {
                $binnacle->update(
                    array(
                        'expires' => (time() + (7 * 24 * 60 * 60))
                    ),
                    array(
                        '=' => array(
                            '_id' => $response['user']
                        )
                    )
                );
                if($binnacle->execute()){
                    $users->findOne("_id = {$response['user']}");
                    $this->logged = true;
                    return array_merge(
                            $users->execute(),
                            array(
                                'binnacle' => $response
                            )
                        );
                }else {
                    session_destroy();
                    throw new APIException('Error',"Problem update binnacle");
                    return false;  
                }
            }else {
                $this->logged = false;
                $binnacle->update(
                    array(
                        'status' => 1
                    ),
                    array(
                        '=' => array(
                            'token' => $this->token,
                            'tokensystem' => $this->tokenSystem
                        )
                    )
                );
                if($binnacle->execute()){
                    session_destroy();
                    return false;
                }else {
                    session_destroy();
                    throw new APIException('Error',"Problem destroy session update binnacle");
                    return false;   
                }
            }
        }

        private function setToken(array $token) {
            $_SESSION = $token;
            $this->token = $token['token'];
            $this->tokenSystem = $token['tokenSystem'];
        }

        private function setBinnacle(string $user, $expires = false) {
            $binnacle = new Handler_SQLBuilder('table_binnacle');

            $binnacle->insert(array(
                'token' => $this->token,
                'tokensystem' => $this->tokenSystem,
                'user' => $user,
                'expires' => $expires, 
                'fech_create' => time(),
                'ip' => $_SERVER['REMOTE_ADDR'],
                'operator' => $_SERVER['HTTP_USER_AGENT'],
                'status' => 0
            ));
            return $binnacle->execute();
        }
        
        public static function __in(array $data, $user) {
            if($user){
                return array(
                    'err' => 1,
                    'm' => 'You on Started session',
                    'status' => 502
                );
            }    

            $t = new self();
            $users = new Handler_SQLBuilder('table_users');

            if(isset($data['mail']) && isset($data['password'])) {
                $users->findOne("mail = '{$data['mail']}'");
                $response = $users->execute();
                
                if($response && count($response)>0) {
                    if(password_verify($data['password'],$response['password'])) {
                        $t->token = password_hash(
                            $data['password'],
                            PASSWORD_BCRYPT
                        );
                        $t->tokenSystem = password_hash(
                            $response['password'].time().$data['password'],
                            PASSWORD_BCRYPT
                        );
                        $t->setToken(array('token' => $t->token ,'tokenSystem' => $t->tokenSystem));
                        
                        $users->update(
                            array(
                                'online' => 0
                            ),
                            "_id = '{$response['_id']}'"
                        );

                        if($users->execute()){
                            $expires = (time() + (7 * 24 * 60 * 60)); // tiempo de vida de las session que se toma con time sumandole 7 dias.
    
                            if($t->setBinnacle($response['_id'], $expires))
                                return array(
                                    'err' => 0,
                                    'token' => $t->token,
                                    'tokensystem' => $t->tokenSystem,
                                    'expires' => $expires,
                                    'm' => 'OK',
                                    'status' => 200
                                );
                        }else return array(
                                'err' => 1,
                                'm' => 'Not execute update users',
                                'status' => 400
                            );

                    }else return array(
                            'err' => 1,
                            'm' => 'Password Invalid, (Contraseña Invalida)',
                            'status' => 404
                        ); 
                }else return array(
                        'err' => 1,
                        'm' => 'Not found User, (Usuario no Existente)',
                        'status' => 404
                    );
            }else return array(
                    'err' => 1,
                    'm' => 'Not sended params for start logged, (No enviaste parametro para iniciar sesion)',
                    'status' => 400
                );
        }

        public static function __register(array $body, $user) {
            if(!is_array($user) && $user==='false')    
                return array(
                    'err' => 1,
                    'm' => 'You on Started session',
                    'status' => 502
                );
                
            if($t = new self()){
                if(is_array($body)){
                    $users = new Handler_SQLBuilder('table_users');
 
                    if(!$body['name'])
                        return array(
                            'err' => 1,
                            'm' => 'Not Found name',
                            'status' => 323
                        );
                    if(!$body['lastname'])
                        return array(
                            'err' => 1,
                            'm' => 'Not Found lastname',
                            'status' => 323
                        );
                    if(!$body['nameCompany'])
                        return array(
                            'err' => 1,
                            'm' => 'Not Found nameCompany',
                            'status' => 323
                        );
                    if(!$body['address'])
                        return array(
                            'err' => 1,
                            'm' => 'Not Found address',
                            'status' => 323
                        );
                    if(!$body['mail'])
                        return array(
                            'err' => 1,
                            'm' => 'Not Found mail',
                            'status' => 323
                        );
                    if(!$body['password'])
                        return array(
                            'err' => 1,
                            'm' => 'Not Found password',
                            'status' => 323
                        );
                    if(!$body['page_url'])
                        return array(
                            'err' => 1,
                            'm' => 'Not Found page_url',
                            'status' => 323 
                        );
                    if(!$body['about_me'])
                        return array(
                            'err' => 1,
                            'm' => 'Not Found page_url',
                            'status' => 323 
                        );
                    if(!$body['photo'] && !is_array($body['photo']))
                        return array(
                            'err' => 1,
                            'm' => 'Not Found photo',
                            'status' => 325
                        );
                    if(!$body['picture'] && !is_array($body['picture']))
                        return array(
                            'err' => 1,
                            'm' => 'Not Found picture',
                            'status' => 325
                        );
                    if(!$body['dni'] && !is_int($body['dni'])) 
                        return array(
                            'err' => 1,
                            'm' => 'You dni input is not valid, Format number',
                            'status' => 325
                        ); 
                    if(!$body['phone'] && !is_int($body['phone']))
                        return array(
                            'err' => 1,
                            'm' => 'You phone is not valid, Format number',
                            'status' => 325
                        );
                    if(!$body['mobile'] && !is_int($body['mobile']))
                        return array(
                            'err' => 1,
                            'm' => 'You mobile is not valid, Format number',
                            'status' => 325
                        );
                    if(!$body['localities'] && !is_array($body['localities']))
                        return array(
                            'err' => 1,
                            'm' => 'You localities is no valid, Format is array',
                            'status' => 325
                        );
                    if(!$body['state'] && !is_int($body['state']))
                        return array(
                            'err' => 1,
                            'm' => 'You state is no valid, Format is number',
                            'status' => 325
                        );
                    if(!$body['status'] && !is_int($body['status']))
                        return array(
                            'err' => 1,
                            'm' => 'You status is no valid, Format is number',
                            'status' => 325
                        );
                    if(!$body['priv'] && !is_int($body['priv']))
                        return array(
                            'err' => 1,
                            'm' => 'You priv is no valid, Format is number',
                            'status' => 325
                        );

                    if($body['name'] && $body['lastname'] && $body['nameCompany'])
                        $body['displayName'] = strtolower($body['name']).strtolower($body['lastname']).strtolower($body['nameCompany']);

                    if($body['password'])
                        $body['password'] = password_hash($body['password'] ,PASSWORD_BCRYPT);    
                    
                    $users->findone(" mail = '{$body['mail']}' OR dni = '{$body['dni']}' ");
                    $response = $users->execute();
                    if($response && count($response)>0)
                        return array(
                            'err' => 1,
                            'm' => 'User existed',
                            'status' => 401
                        );
                    
                    $users->insert($body);
                    if($response = $users->execute())
                        return array(
                            'err' => 0,
                            'm' => 'OK',
                            '_id' => $response,
                            'status' => 200 
                        );

                }else {
                    throw new APIException('Error:','This token is not array');
                    return false;
                }
            }else return array(
                'err' => 1,
                'm' => 'You on Started session',
                'status' => 502
            );
        }

        public static function __out($token, $tokensystem, $user) {
            if(!is_array($user) || !$user)
                return array(
                    'err' => 1,
                    'm' => 'Not start session for loggeout',
                    'status' => 501
                );

            $t = new self();

            if($t->token === $token && $t->tokenSystem === $tokensystem){
                $binnacle = new Handler_SQLBuilder('table_binnacle');
                $users = new Handler_SQLBuilder('table_users');
    
                $binnacle->update(
                    array(
                        'status' => 1
                    ),
                    array(
                        '=' => array(
                            'token' => $t->token,
                            'tokensystem' => $t->tokenSystem
                        )
                    )
                );
                
                $users->update(
                    array(
                        'online' => 1,
                    ),
                    "_id = '{$t->user['_id']}'"
                );
    
                if($binnacle->execute() && $users->execute()) 
                    return array(
                        'err' => 0,
                        'm' => 'OK',
                        'status' => 200
                    );
            }else {
                session_destroy();
                return array(
                    'err' => 1,
                    'm' => 'Problem in token expiress',
                    'status' => 505
                );
            } 
        }

        protected function __checkToken($token, $tokensystem) {
            if($this->logged){
                if($token === $this->token && $tokensystem === $this->tokenSystem)
                    return true;
                else 
                    return false;
            }else {
                $this->__out();
                return false;
            } 
        }

    }
