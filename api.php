<?php 
	require_once ('server/autoload.php');
	
	use server\core\Handler_HTTP;
	use server\core\Handler_SQLBuilder;
	use server\core\Handler_Authentications;

	$http = new Handler_HTTP;

	$http->post('login',function($req, $res){
		if(!$req['user']){
			$response = Handler_Authentications::__in($req['body'],$req['user']);
			$res($response, $req);
		}else $res(array(
			'err' => 1,
			'm' => 'You on Started session',
			'status' => 502,
		));
	});

	$http->get('logout',function($req, $res){
		if($req['user']){
			if(!$req['query']['token'])
				$res(
					array(
						'err'=> 1,
						'm' => 'Not Found token in params sended',
						'status' => 502
					)
				);
			if(!$req['query']['tokensystem'])
				$res(
					array(
						'err'=> 1,
						'm' => 'Not Found token in params sended',
						'status' => 502
					)
				);
				if(($req['user']['binnacle']['token'] !== $req['query']['token']) 
						|| 
						($req['user']['binnacle']['tokensystem'] !== $req['query']['tokensystem'])
					)
					$res(
						array(
							'err' => 1,
							'm' => 'Token invalid'
						)
					);

			$response = Handler_Authentications::__out(
					$req['query']['token'],
					$req['query']['tokensystem'],
					$req['user']
			);
			$res($response);
			
		}else $res(
			array(
				'err' => 1,
				'm' => 'Not logged',
				'status' => 501
			)
		);
	});

	$http->post('register',function($req, $res){
		$body = $req['body'];
		$user = $req['user'];

		if($user)
			return $res(array(
				'err' => 1,
				'm' => 'You on Started session',
				'status' => 502
			));

		if(!is_array($body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended params for start operation register',
				'status' => 400
			));

		if(!array_key_exists('name', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param name',
				'status' => 400
			));
		
		if(!array_key_exists('lastname', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param lastname',
				'status' => 400
			));

		if(!array_key_exists('dni', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param dni',
				'status' => 400
			));

		if(!array_key_exists('phone', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param phone',
				'status' => 400
			));
		if(!array_key_exists('mobile', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param mobile',
				'status' => 400
			));
		if(!array_key_exists('address', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param address',
				'status' => 400
			));
		if(!array_key_exists('localities', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param localities',
				'status' => 400
			));
		if(!array_key_exists('mail', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param mail',
				'status' => 400
			));
		if(!array_key_exists('password', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param password',
				'status' => 400
			));
		if(!array_key_exists('photo', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param photo',
				'status' => 400
			));
		if(!array_key_exists('picture', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param picture',
				'status' => 400
			));
		if(!array_key_exists('page_url', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param namapge_url',
				'status' => 400
			));
		if(!array_key_exists('about_me', $body))
			return $res(array(
				'err' => 1,
				'm' => 'Not sended param name',
				'status' => 400
			));
		if(!array_key_exists('state', $body))
			$body['state'] = 0;

		if(!array_key_exists('status', $body))
			$body['status'] = 0;

		if(!array_key_exists('priv', $body))
			$body['priv'] = 0;
		
		$response = Handler_Authentications::__register($body, $user);
			
		if($response && is_array($response) && count($response))
			return $res($response);
		else 
			return $res(array(
				'err' => 1,
				'm' => 'Error in register user new',
				'status' => 404
			));
	});
	
	$http->get('users/id/:id',function($req, $res) {
		if($req['user']){
			$collection = new Handler_SQLBuilder('table_users');
			$collection->findOne("_id = '{$req['params']['id']}'");
			$response = $collection->execute();
			$res($response);
		}else $res(
			array(
				'err' => 1,
				'm' => 'Not logged',
				'status' => 501
			)
		);

	});

	$http->get('users', function($req, $res) {
		if($req['user']){
			$collection = new Handler_SQLBuilder('table_users');
			$collection->find();
			$response = $collection->execute();
			$res($response);
		}else $res(
			array(
				'err' => 1,
				'm' => 'Not logged',
				'status' => 501
			)
		);
	});

	$http->post('users',function($req, $res) {
		if($req['user']){
			$collection = new Handler_SQLBuilder('table_users');
			$collection->insert($req['body']);
			$response = $collection->execute();
			$res($response);
		}else $res(
			array(
				'err' => 1,
				'm' => 'Not logged',
				'status' => 501
			)
		);
	});

	$http->put('users/id/:id',function($req, $res) { 
		if($req['user']){
			$collection = new Handler_SQLBuilder('table_users');
			$collection->update($req['body'], "_id = '{$req['params']['_id']}'");
			$response = $collection->execute();
			$res($response);
		}else $res(
			array(
				'err' => 1,
				'm' => 'Not logged',
				'status' => 501
			)
		);
	});

	$http->delete('users/id/:id',function($req, $res) {
		if($req['user']){
			$collection = new Handler_SQLBuilder('table_users');
			$collection->remove("_id = '{$req['params']['_id']}'");
			$response = $collection->execute();
			$res($response);
		}else $res(
			array(
				'err' => 1,
				'm' => 'Not logged',
				'status' => 501
			)
			);
	});

	$http->use(function($req,$res){
		$res(array(
			'status' => 404,
			'm' => 'Not Found Operation'
		));
	});
